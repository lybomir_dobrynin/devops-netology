# Домашнее задание к занятию "5.4. Оркестрация группой Docker контейнеров на примере Docker Compose"

>Cперва установил через `apt install packer`, потом заменил на файл с облака яндекса свежий.

>Error: Failed to query available provider pakages

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/1.jpg?raw=true)


## Задача 1

Создать собственный образ операционной системы с помощью Packer.

Для получения зачета, вам необходимо предоставить:
- Скриншот страницы, как на слайде из презентации (слайд 32).

>Устанавливаем `yc`, после чего `yc init`. Необходимо создать профиль, выбрать облако и папку.  Создаем сеть `yc vpc network create --name net` и подсеть сети `yc vpc subnet create --name subnet --zone ru-central1-a --range 10.0.0.0/24 --network-name net --description "netology first subnet"`. Добавляем в конфигурационный файл id и folder_id подсети. Проверяем правильность командой `packer validate centos-7-base.json` и запускаем установку `packer build centos-7-base.json`. После развертывания можно увидеть доступные образы командой `yc compute image list`. После создания образа надо удалить сеть и подсеть, чтобы не выйти за лимиты `yc vpc subnet delete --name subnet && yc vpc network delete --name net`. 

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/5.4.1.png?raw=true)


## Задача 2

Создать вашу первую виртуальную машину в Яндекс.Облаке.

Для получения зачета, вам необходимо предоставить:
- Скриншот страницы свойств созданной ВМ, как на примере

>Необходимо создать сервисный аккаунт `yc iam service-account create --name sa` и добавить роль editor, после чего создаем keyfile `yc iam key create --service-account-name sa --output key.json`. Далее заполняем variables файл, меняя ключи. Убедиться что в системе сформирован ключ `id_rsa.pub`. Проверяем валидность командой `terraform validate`, проверяем что будет сделано командой `terraform plan` и запускаем развертывание VM `terraform apply -auto-approve`.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/5.4.2.png?raw=true)


## Задача 3

Создать ваш первый готовый к боевой эксплуатации компонент мониторинга, состоящий из стека микросервисов.

Для получения зачета, вам необходимо предоставить:
- Скриншот работающего веб-интерфейса Grafana с текущими метриками, как на примере

>В файле inventory необходимо указать публичный ip и выполнить `ansible-playbook provision.yml`, после чего проверяем графану на порту 3000.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/5.4.3.png?raw=true)

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/5.4.4.png?raw=true)


## Задача 4 (*)

Создать вторую ВМ и подключить её к мониторингу развёрнутому на первом сервере.

Для получения зачета, вам необходимо предоставить:
- Скриншот из Grafana, на котором будут отображаться метрики добавленного вами сервера.
---

>#ЗАМЕТКА НА БУДУЩЕЕ: Там основное это порты пробросить на второй вм и в конфиге prometheus первой ноды добавить инфу о nodeexporter и cadvisor второй ноды. Я сделал колхозно, т.к. настроил через внешний адрес, по хорошему в одной локальной сетке это все по хостнейму должно работать
```
    scrape_configs:
    - job_name: 'nodeexporter1'
        scrape_interval: 5s
        static_configs:
        - targets: [ 'nodeexporter:9100' ]
            labels:
            instance: 'nodeexporter01-9100'

    - job_name: 'nodeexporter2'
        scrape_interval: 5s
        static_configs:
        - targets: [ '51.250.79.105:9100' ]
            labels:
            instance: 'nodeexporter02-9100'

    - job_name: 'cadvisor'
        scrape_interval: 5s
        static_configs:
        - targets: ['cadvisor:8080']

    - job_name: 'cadvisor2'
        scrape_interval: 5s
        static_configs:
        - targets: ['51.250.79.105:8080']
```

>После окончания удалить все существующие ресурсы, чтобы деньги не расходовались впустую - `terraform destroy -auto-approve`. Удаляем образ `yc compute image delete --id <id_image>`.