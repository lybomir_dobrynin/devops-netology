# Домашнее задание к занятию "08.01 Введение в Ansible"

## Подготовка к выполнению
1. Установите ansible версии 2.10 или выше.
2. Создайте свой собственный публичный репозиторий на github с произвольным именем.
3. Скачайте [playbook](./playbook/) из репозитория с домашним заданием и перенесите его в свой репозиторий.

## Основная часть
1. Попробуйте запустить playbook на окружении из `test.yml`, зафиксируйте какое значение имеет факт `some_fact` для указанного хоста при выполнении playbook'a.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.1.png?raw=true)

2. Найдите файл с переменными (group_vars) в котором задаётся найденное в первом пункте значение и поменяйте его на 'all default fact'. `vi ./playbook/group_vars/all/examp.yml`

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.2.png?raw=true)

3. Воспользуйтесь подготовленным (используется `docker`) или создайте собственное окружение для проведения дальнейших испытаний.

>`docker run -d -it --name ubuntu ubuntu:20.04 && docker run -d -it --name centos centos`, после чего необходимо добавить в ubuntu python, установил внутри контейнера для выполнения дз, без сборки образа.

4. Проведите запуск playbook на окружении из `prod.yml`. Зафиксируйте полученные значения `some_fact` для каждого из `managed host`.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.3.png?raw=true)

5. Добавьте факты в `group_vars` каждой из групп хостов так, чтобы для `some_fact` получились следующие значения: для `deb` - 'deb default fact', для `el` - 'el default fact'.
6. Повторите запуск playbook на окружении `prod.yml`. Убедитесь, что выдаются корректные значения для всех хостов.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.5.png?raw=true)

7. При помощи `ansible-vault` зашифруйте факты в `group_vars/deb` и `group_vars/el` с паролем `netology`.

>`ansible-vault encrypt group_vars/deb/examp.yml && ansible-vault encrypt group_vars/el/examp.yml`

8. Запустите playbook на окружении `prod.yml`. При запуске `ansible` должен запросить у вас пароль. Убедитесь в работоспособности.

>`ansible-playbook -i inventory/prod.yml site.yml --ask-vault-pass`

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.8.png?raw=true)

9. Посмотрите при помощи `ansible-doc` список плагинов для подключения. Выберите подходящий для работы на `control node`.

>`ansible-doc -t connection --list` -> local (execute on controller)

10. В `prod.yml` добавьте новую группу хостов с именем  `local`, в ней разместите localhost с необходимым типом подключения.

```yaml
local:
  hosts:
    localhost:
      ansible_connection: local
```

11. Запустите playbook на окружении `prod.yml`. При запуске `ansible` должен запросить у вас пароль. Убедитесь что факты `some_fact` для каждого из хостов определены из верных `group_vars`.

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.11.png?raw=true)

12. Заполните `README.md` ответами на вопросы. Сделайте `git push` в ветку `master`. В ответе отправьте ссылку на ваш открытый репозиторий с изменённым `playbook` и заполненным `README.md`.

## Необязательная часть

1. При помощи `ansible-vault` расшифруйте все зашифрованные файлы с переменными.

>`ansible-vault decrypt group_vars/deb/examp.yml`

2. Зашифруйте отдельное значение `PaSSw0rd` для переменной `some_fact` паролем `netology`. Добавьте полученное значение в `group_vars/all/exmp.yml`.

>`ansible-vault encrypt_string 'PaSSw0rd' --name 'some_fact' > group_vars/all/examp.yml`

3. Запустите `playbook`, убедитесь, что для нужных хостов применился новый `fact`.

>Запуск без указания пароля

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.x.3.1.png?raw=true)

>Запуск с ключом `--ask-vault-pass`

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.x.3.2.png?raw=true)

4. Добавьте новую группу хостов `fedora`, самостоятельно придумайте для неё переменную. В качестве образа можно использовать [этот](https://hub.docker.com/r/pycontribs/fedora).

>![dashboard](https://gitlab.com/lybomir_dobrynin/devops-netology/-/raw/main/Homeworks/.img/8.1.x.4.png?raw=true)

5. Напишите скрипт на bash: автоматизируйте поднятие необходимых контейнеров, запуск ansible-playbook и остановку контейнеров.

```bash
    docker run -d -it --name ubuntu ubuntu:22.04 && \
    docker exec -it ubuntu apt-get update && \
    docker exec -it ubuntu apt-get install python3-pip -y

    docker run -d -it --name centos7 centos

    docker run -d -it --name fedora pycontribs/fedora

    ansible-playbook site.yml -i inventory/ --ask-vault-pass

    docker stop ubuntu && docker rm ubuntu
    docker stop centos7 && docker rm centos7
    docker stop fedora && docker rm fedora
```

6. Все изменения должны быть зафиксированы и отправлены в вашей личный репозиторий.