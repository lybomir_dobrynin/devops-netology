variable "cloud_id" {
  default = "b1gft7onlquqaa905o3i"
}

variable "folder_id" {
  default = "b1gsv3rtu1q428jd7g23"
}

variable "zone" {
  default = "ru-central1-a"
}